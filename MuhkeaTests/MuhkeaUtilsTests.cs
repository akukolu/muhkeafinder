﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MuhkeaFinder;

namespace MuhkeaTests
{
    [TestClass]
    public class MuhkeaUtilsTests
    {
        [TestMethod]
        public void MapWordToInt32ConvertsWordToBinaryCorrectly()
        {
            //happy path
            Assert.AreEqual(1, MuhkeaUtils.MapWordToInt32("a"));
            Assert.AreEqual(2, MuhkeaUtils.MapWordToInt32("b"));
            Assert.AreEqual(Convert.ToInt32("11", 2), MuhkeaUtils.MapWordToInt32("ab"));
            Assert.AreEqual(Convert.ToInt32("11111111111111111111111111111", 2), MuhkeaUtils.MapWordToInt32("abcdefghijklmnopqrstuvwxyzåäö"));
            Assert.AreEqual(Convert.ToInt32("11111111111111111111111111111", 2), MuhkeaUtils.MapWordToInt32("öäåzyxwvutsrqponmlkjihgfedcba"));
            Assert.AreEqual(Convert.ToInt32("00010000000000100000010000100", 2), MuhkeaUtils.MapWordToInt32("choz"));
            Assert.AreEqual(Convert.ToInt32("10000000000000000000000000001", 2), MuhkeaUtils.MapWordToInt32("öa"));

            //duplicate characters
            Assert.AreEqual(1, MuhkeaUtils.MapWordToInt32("aaaaaa"));
            Assert.AreEqual(Convert.ToInt32("00000000010000001000100000001", 2), MuhkeaUtils.MapWordToInt32("aatami"));

            //illegal characters in string
            Assert.AreEqual(Convert.ToInt32("10000000000000000000000000001", 2), MuhkeaUtils.MapWordToInt32(" ö  !¤#A Öa &?!<"));
            Assert.AreEqual(Convert.ToInt32("00010000010100100100001000001", 2), MuhkeaUtils.MapWordToInt32(" Ol1o4!tar ¤& gz"));

            //no matches
            Assert.AreEqual(0, MuhkeaUtils.MapWordToInt32(" 14¤&)/(64?? !! 2..-"));
        }

        [TestMethod]
        public void CountUniqueLettersReturnsCorrectNumberWithGivenIntValue()
        {
            Assert.AreEqual(1, MuhkeaUtils.CountBits(1));
            Assert.AreEqual(2, MuhkeaUtils.CountBits(3));
            Assert.AreEqual(29, MuhkeaUtils.CountBits(Convert.ToInt32("11111111111111111111111111111", 2)));
            Assert.AreEqual(15, MuhkeaUtils.CountBits(Convert.ToInt32("01010101010101010101010101011", 2)));
            Assert.AreEqual(4, MuhkeaUtils.CountBits(Convert.ToInt32("00000010000000100000000100001", 2)));
            Assert.AreEqual(5, MuhkeaUtils.CountBits(Convert.ToInt32("111101", 2)));
            Assert.AreEqual(31, MuhkeaUtils.CountBits(Int32.MaxValue));
        }

        [TestMethod]
        public void RemoveSpecialCharactersCorrectlyFiltersWord()
        {
            Assert.AreEqual("Testi Testi -- TESTI testi", MuhkeaUtils.RemoveSpecialCharacters("Testi. \"Testi\",!#¤%&/(=? -- _<>´`''TESTI testi"));
            Assert.AreEqual(" 1464  2", MuhkeaUtils.RemoveSpecialCharacters(" 14¤&)/(64?? !! 2.."));
            Assert.AreEqual(String.Empty, MuhkeaUtils.RemoveSpecialCharacters(String.Empty));
        }
    }
}
