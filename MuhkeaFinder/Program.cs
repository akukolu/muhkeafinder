﻿using System;

namespace MuhkeaFinder
{
    class Program
    {
        private const string FilePath = "Resources/alastalon_salissa.txt";

        static void Main()
        {
            var gatherer = new MuhkeaGatherer();
            gatherer.Gather(FilePath);
            gatherer.FindWords();
            PrintResults(gatherer);
        }

        private static void PrintResults(MuhkeaGatherer gatherer)
        {
            Console.WriteLine("Unique letter count: {0}", gatherer.UniqueAlphabetCount);
            Console.WriteLine("Word pairs found: {0}", gatherer.WordPairs.Count);
            Console.WriteLine("\nMatching words:");
            foreach (var wordpair in gatherer.WordPairs)
            {
                Console.WriteLine("\t" + wordpair);
            }
            Console.Write("\nPress any key...");
            Console.ReadLine();
        }
    }
}
