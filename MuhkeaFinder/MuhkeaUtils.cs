﻿using System;

namespace MuhkeaFinder
{
    public static class MuhkeaUtils
    {
        private static readonly char[] AllowedCharacters =
        {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z', 'å', 'ä', 'ö'
        };

        /// <summary>
        /// Converts a word to Int32 representation with the following logic:
        ///   - If a letter exists in word, the bit corresponding the letter's 
        ///     placement in AllowedCharacters -array is flagged from 0 to 1
        ///   - Letter's binary flag is determined by array placement, index
        ///     0 (character 'a') would be 2^0, index 1 ('b') would be 2^1 and
        ///     so on.
        /// </summary>
        /// <param name="word">Word to be converted</param>
        /// <returns>Integer representation of the word</returns>
        public static int MapWordToInt32(string word)
        {
            var value = 0;
            var letters = word.ToCharArray();
            foreach (var l in letters)
            {
                var index = Array.IndexOf(AllowedCharacters, l);
                if (index != -1)
                {
                    value |= 1 << index;
                }

            }
            return value;
        }

        /// <summary>
        /// Count the number of bits (flagged to 1) in a given integer.
        /// </summary>
        /// <param name="value">Integer where the number of bits will be determined.</param>
        /// <remarks>See Hamming weight for details: http://en.wikipedia.org/wiki/Hamming_weight </remarks>
        /// <returns>Number of bits in a given integer.</returns>
        public static int CountBits(int value)
        {
            value = value - ((value >> 1) & 0x55555555);
            value = (value & 0x33333333) + ((value >> 2) & 0x33333333);
            return (((value + (value >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
        }

        /// <summary>
        /// Removes special characters from string (including everything else except letters, digits, whitespaces or dashes)
        /// </summary>
        /// <param name="word">string where special characters need to be removed</param>
        /// <returns>filtered string</returns>
        public static string RemoveSpecialCharacters(string word)
        {
            var array = word.ToCharArray();
            array = Array.FindAll(array, (c => (char.IsLetterOrDigit(c) || char.IsWhiteSpace(c) || c == '-')));
            return new String(array);
        }
    }
}
