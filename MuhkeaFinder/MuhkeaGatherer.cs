﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MuhkeaFinder
{
    public class MuhkeaGatherer
    {
        public struct Word
        {
            public string Name;
            public int Value;
            public int BitCount;
        }

        public List<Word> WordList = new List<Word>();
        public List<string> WordPairs = new List<string>();
        public int UniqueAlphabetCount { get; set; }

        /// <summary>
        /// Gather unique words from a specified file and sort them by unique alphabet count, descending
        /// </summary>
        /// <param name="filePath">file path</param>
        public void Gather(string filePath)
        {
            try
            {
                using (var sr = new StreamReader(filePath))
                {
                    var text = sr.ReadToEnd().ToLower();
                    var words = MuhkeaUtils.RemoveSpecialCharacters(text).Split().ToArray().Distinct();
                    foreach (var i in words)
                    {
                        var value = MuhkeaUtils.MapWordToInt32(i);
                        if (value > 0)
                        {
                            WordList.Add(new Word { Name = i, Value = value, BitCount = MuhkeaUtils.CountBits(value) });
                        }
                    }
                    WordList.Sort((w1, w2) => w1.BitCount.CompareTo(w2.BitCount));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Get the wordpairs with maximum combined unique alphabet count.
        /// </summary>
        /// <remarks>The list needs to be ordered by single word alphabet count</remarks>
        public void FindWords()
        {
            var words = WordList.ToArray();
            for (var i = 0; i < words.Length; i++)
            {
                for (var j = i + 1; j < words.Length; j++)
                {
                    //if max possible value is less than current maximum, we can ignore the rest
                    if ((words[i].BitCount + words[j].BitCount) < UniqueAlphabetCount)
                    {
                        break;
                    }
                    CompareWords(words[i], words[j]);
                }
            }
        }

        /// <summary>
        /// Get the combined unique alphabet count from two words and compare it to
        /// the current highest maximum. Store word pairs with highest alphabet count
        /// to a list.
        /// </summary>
        /// <param name="a">First word to be compared</param>
        /// <param name="b">Second word to be compared</param>
        private void CompareWords(Word a, Word b)
        {
            var val = MuhkeaUtils.CountBits(a.Value | b.Value);
            if (val < UniqueAlphabetCount)
            {
                return;
            }
            if (val > UniqueAlphabetCount)
            {
                UniqueAlphabetCount = val;
                WordPairs.Clear();
            }
            WordPairs.Add(String.Format("{0} -- {1}", a.Name, b.Name));
        }
    }
}
